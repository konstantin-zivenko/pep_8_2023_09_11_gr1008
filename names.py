first_name = "Kostiantyn"
last_name = "Zivenko"

print(f"{first_name} {last_name}, hi!")

# snace_case
#
# MyClass
#
# CONSTANT, MY_CONSTANT
#
# module.py, my_module.py
#
# package, mypackage


def db(x):
    return x * 2


def multiply_by_two(x):
    return x * 2


def function(arg_one: int, agr_two: str, arg_three: float, arg_four: str):
    """It is my docstring.

    """
    return arg_one


# from mypkg import example_1, example_2, example_3, example_4


# total = first_variamle + second_variable - third_variable


x = 3
if x > 5:
    # Start of my comment
    # end.
    #
    # It is ...
    print("x is lager than 5")


x = 3
if x > 5 and x < 10:
    print("x is lager than 5")  # Comment.

list_of_numbers = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
]

y = x**2 + 5
z = (x + y) * (x - y)

y = x**2 + 5
z = (x + y) * (x - y)
